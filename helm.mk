.PHONY: helm-clean helm-lint helm-render helm-package helm-upload

_HELM_USR= # User id to the repository where the helm package is uploaded
_HELM_PSW= # Password to the repository where the helm package is uploaded
_HELM_PATH= # Chart path

helm-lint:
	helm lint

helm-package: helm-clean
	helm package $(PWD)

helm-upload:
	curl -u "$(_HELM_USR):$(_HELM_PSW)" https://nexus.gwdc.org.au/repository/helm/ --upload-file `ls *.tgz` -v

helm-values:
	@helm show values $(_HELM_PATH)

helm-render:
	@helm template $(_HELM_PATH)

helm-clean:
	rm -rf *.tgz