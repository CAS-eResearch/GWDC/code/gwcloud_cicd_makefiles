# gwcloud_cicd_makefiles
Makefile library repository.

## Motivation
To centralise common tasks of technologies in scope while following a DevSecOps workflow.

| Operation | Comment |
| --- | --- |
| lint | Syntax analysis. Promotes code quality |
| build | Application build process. |
| test | Unit tests. |
| package | Application packaging. |
| publish | Documentation publishing. |
|||

## System prep
```bash
# While in the root path of a repository, execute the following:
git submodule add https://gitlab.com/CAS-eResearch/GWDC/code/gwcloud_cicd_makefiles.git .make

# Syncronise submodule
git submodule update --init --force --recursive
```

## Usage
```bash
# Generate Makefile in repository's root directory
cat << 'EOF' >> Makefile
# include base makefile
include .make/base.mk

# include makefiles to be inherited
include .make/sample.mk

# include repo specific override residing in current repository
-include Override.mk

# include workstation specific targets
-include WorkstationTargets.mk
EOF

# Generate .gitignore file with workstation specific files
cat << 'EOF' >> .gitignore
# Ignore temporary files
*.tmp

# Ignore workstation local make targets
WorkstationTargets.mk
EOF
```