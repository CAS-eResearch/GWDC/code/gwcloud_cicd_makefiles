.PHONY: make submodule

submodule:
	git submodule init
	git submodule update --init --recursive --remote

make: submodule 

# Populate common env vars
ifeq ($(strip $(PROJECT_NAME)),)
  PROJECT_NAME=$(shell basename $(CURDIR))
endif

.PHONY: env-print-app
env-print-app:
	$(call printvar,PROJECT_NAME)

define printvar
	@echo "$(1): $($(1))"
endef
